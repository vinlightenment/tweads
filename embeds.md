# YouTube testing:

Embed sample snippets provided by YouTube directly

<iframe width="560" height="315" src="https://www.youtube.com/embed/Ez5xPMwHnXI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Same as above but with privacy enhancements
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Ez5xPMwHnXI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<iframe width="560" height="315" src="https://www.youtube.com/embed/QUXsXRYVdFc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

YouTube snippet copied from WordPress:

<figure class="wp-block-embed is-type-rich is-provider-youtube wp-block-embed-youtube wp-embed-aspect-16-9 wp-has-aspect-ratio"><div class="wp-block-embed__wrapper">
<div class="jetpack-video-wrapper"><span class="embed-youtube" style="text-align:center; display: block;"><iframe class='youtube-player' width='739' height='416' src='https://www.youtube.com/embed/nDrmUeA8qnw?version=3&#038;rel=1&#038;showsearch=0&#038;showinfo=1&#038;iv_load_policy=1&#038;fs=1&#038;hl=en&#038;autohide=2&#038;wmode=transparent' allowfullscreen='true' style='border:0;' sandbox='allow-scripts allow-same-origin allow-popups allow-presentation'></iframe></span></div>
</div><figcaption>Zero-emissions vessel design incorporating wind, hydrogen, solar etc.</figcaption></figure>

YouTube snippet copied from StackOverflow:
<iframe width="560" height="315"
src="https://www.youtube.com/embed/MUQfKFzIOeU" 
frameborder="0" 
allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
allowfullscreen></iframe>

Tweet sample snippet provided by Twitter directly:
<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Farmland reverts to woodland.<br><br>Nobody planted these trees or decided what spacing to put them at or mantained them as they grew.<br><br>Given enough time, they sort that stuff out for themselves. <a href="https://t.co/SMhWJKx2fK">pic.twitter.com/SMhWJKx2fK</a></p>&mdash; hometree (@hometree__) <a href="https://twitter.com/hometree__/status/1359055412738334722?ref_src=twsrc%5Etfw">February 9, 2021</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

Tweet snippet copied from WordPress:
<figure class="wp-block-embed is-type-rich is-provider-twitter wp-block-embed-twitter"><div class="wp-block-embed__wrapper">
<div class="embed-twitter"><blockquote class="twitter-tweet" data-width="550" data-dnt="true"><p lang="en" dir="ltr">Did you know? 90% of everything we use has been shipped over sea at some point in its production process. The shipping industry emits more greenhouse gasses than the entire economy of Germany. Be part of the change today!👇<a href="https://href.li/?https://t.co/DPgYYkTCCU" rel="noreferrer">https://t.co/DPgYYkTCCU</a><br>📸  from <a href="https://href.li/?https://t.co/F7CHrPjIDV" rel="noreferrer">https://t.co/F7CHrPjIDV</a> <a href="https://href.li/?https://t.co/cxWJye3N2l" rel="noreferrer">pic.twitter.com/cxWJye3N2l</a></p>&mdash; EcoClipper (@EcoClipper) <a href="https://twitter.com/EcoClipper/status/1335026212041859074?ref_src=twsrc%5Etfw">December 5, 2020</a></blockquote><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></div>
</div></figure>
